package ru.samara.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage {

    public WebDriver driver;
    public WebDriverWait wait;
    public String url;

    public BasePage open() {
        return this;
    }

    public void typeInto(By locator, String text) {
        WebElement element = driver.findElement(locator);
        element.clear();
        element.sendKeys(text);
    }

    public void typeInto(WebElement element, String text) {
        element.clear();
        element.sendKeys(text);
    }

    public void click(By locator) {
        driver.findElement(locator).click();
    }

    public void click(WebElement element) {
        element.click();
    }

    public void submit(By locator) {
        driver.findElement(locator).submit();
    }

    public void submit(WebElement element) {
        element.submit();
    }

}
