package ru.samara.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class LoginPage extends BasePage {

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    By emailInputLocator = By.cssSelector("input[type='email']");
    By passwordInputLocator = By.cssSelector("input[type='password']");
    By loginButtonLocator = By.cssSelector("#login");

    public BoardsListPage loginAs(String email, String password) {
        typeInto(emailInputLocator, email);
        typeInto(passwordInputLocator, password);
        submit(loginButtonLocator);
        return new BoardsListPage(driver);
    }

}
