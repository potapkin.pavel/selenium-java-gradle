package ru.samara.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class BoardPage extends BasePage {

    public BoardPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 5);
    }

    By listLocator = By.cssSelector(".js-list-content");
    By addListButtonLocator = By.cssSelector(".js-add-list");
    By listNameInputLocator = By.cssSelector(".list-name-input");
    By saveListEditButtonLocator = By.cssSelector(".js-save-edit");
    By listEditButtonLocator = By.cssSelector(".list-header-extras-menu");
    By archiveListButtonLocator = By.cssSelector(".js-close-list");

    By cardLocator = By.cssSelector("a.list-card");
    By addCardButtonLocator = By.cssSelector(".js-add-a-card");
    By addAnotherCardButtonLocator = By.cssSelector(".js-add-another-card");
    By cardTitleTextAreaLocator = By.cssSelector(".list-card-composer-textarea");
    By saveCardEditButtonLocator = By.cssSelector(".js-add-card");

    By boardLocator = By.cssSelector("#board");


    public void addList(String listName) {
        click(addListButtonLocator);
        typeInto(listNameInputLocator, listName);
        click(saveListEditButtonLocator);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("div.is-subscribe-shown")));
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("a.icon-sm.icon-template-card")));
    }

    public List<WebElement> getLists() {
        return driver.findElements(listLocator);
    }

    public List<WebElement> getCards() {
        return driver.findElements(cardLocator);
    }

    public WebElement getListByTitle(String listTitle) {
        return driver.findElement(By.xpath("//div[contains(@class, 'js-list-content') and contains(., '" + listTitle + "')]"));
    }

    public WebElement getCardByTitle(String cardTitle) {
        return driver.findElement(By.xpath("//a[contains(@class, 'list-card') and contains(., '" + cardTitle + "')]"));
    }

    public void archiveList(WebElement list) {
        click(list.findElement(listEditButtonLocator));
        click((archiveListButtonLocator));
    }

    public void archiveAllLists() {
        int listCount = getLists().size();
        while(listCount > 0) {
            archiveList(driver.findElement(listLocator));
            wait.until(ExpectedConditions.numberOfElementsToBe(listLocator, listCount - 1));
            listCount -= 1;
        }
    }

    public void addCard(String cardTitle) {
        click(addCardButtonLocator);
        typeInto(cardTitleTextAreaLocator, cardTitle);
        click(saveCardEditButtonLocator);
        click(boardLocator);
    }

    public void addAnotherCard(String cardTitle) {
        click(addAnotherCardButtonLocator);
        typeInto(cardTitleTextAreaLocator, cardTitle);
        click(saveCardEditButtonLocator);
        click(boardLocator);
    }

    public void editCard(String cardTitle, String text) {
        click(getCardByTitle(cardTitle));
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".js-loading-card-actions")));
        click(driver.findElement(By.cssSelector(".description-content")));
        typeInto(driver.findElement(By.cssSelector(".description.card-description")), text);
        click(driver.findElement(By.cssSelector(".primary.confirm.mod-submit-edit.js-save-edit")));
        click(By.cssSelector(".dialog-close-button"));
    }

}
