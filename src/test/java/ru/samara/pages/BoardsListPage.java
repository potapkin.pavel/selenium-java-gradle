package ru.samara.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class BoardsListPage extends BasePage {

    public BoardsListPage(WebDriver driver) {
        this.driver = driver;
    }

    private By byBoardTitle(String boardTitle){
        return By.xpath("//a[contains(@class, 'board-tile') and contains(., '" + boardTitle + "')]");
    }

    public BoardPage openBoard(String boardTitle) {
        click(byBoardTitle(boardTitle));
        return new BoardPage(driver);
    }

}
