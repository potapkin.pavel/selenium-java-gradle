package ru.samara.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class MainPage extends BasePage {

    public MainPage(WebDriver driver) {
        this.driver = driver;
        url = "https://trello.com/";
    }

    By loginButton = By.cssSelector("a[href='/login']");

    public MainPage open() {
        driver.navigate().to(url);
        return this;
    }

    public LoginPage openLoginPage() {
        click(loginButton);
        return new LoginPage(driver);
    }

}
