package ru.samara.tests;

import com.google.common.io.Resources;
import jdk.nashorn.internal.ir.annotations.Ignore;
import org.testng.annotations.*;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static org.testng.Assert.assertEquals;
import static org.testng.AssertJUnit.assertTrue;

public class MainTest extends BaseTest {

    @Test
    public void testWhenOneListCreatedAndArchived() {
        boardPage.addList("list");
        assertTrue(boardPage.getListByTitle("list").isDisplayed());

        boardPage.archiveAllLists();
        assertEquals(0, boardPage.getLists().size());
    }

    @Test
    public void testWhenThreeListsCreatedAndArchived() {
        boardPage.addList("List1");
        boardPage.addList("List2");
        boardPage.addList("List3");
        assertEquals(3, boardPage.getLists().size());
        assertTrue(
            boardPage.getListByTitle("List1").isDisplayed() &&
            boardPage.getListByTitle("List2").isDisplayed() &&
            boardPage.getListByTitle("List3").isDisplayed());

        boardPage.archiveAllLists();
        assertEquals(0, boardPage.getLists().size());
    }

    @Test
    public void testWhenOneListWithOneCardCreatedAndArchived() {
        boardPage.addList("list");
        assertTrue(boardPage.getListByTitle("list").isDisplayed());

        boardPage.addCard("card");
        assertTrue(boardPage.getCardByTitle("card").isDisplayed());

        boardPage.archiveAllLists();
        assertEquals(0, boardPage.getLists().size());
    }

    @Test
    public void testWhenOneListWithThreeCardsCreatedAndArchived() {
        boardPage.addList("list");
        assertTrue(boardPage.getListByTitle("list").isDisplayed());

        boardPage.addCard("card1");
        boardPage.addAnotherCard("card2");
        boardPage.addAnotherCard("card3");
        assertEquals(3, boardPage.getCards().size());
        assertTrue(
            boardPage.getCardByTitle("card1").isDisplayed() &&
            boardPage.getCardByTitle("card2").isDisplayed() &&
            boardPage.getCardByTitle("card3").isDisplayed());

        boardPage.archiveAllLists();
        assertEquals(0, boardPage.getLists().size());
    }

    @Test
    public void testWhenOneListWithMaxLengthNameCreatedAndArchived() throws IOException {
        String listName = Resources.toString(Resources.getResource("txt\\lorem512.txt"), StandardCharsets.UTF_8);

        boardPage.addList(listName);
        assertTrue(boardPage.getListByTitle(listName.substring(0, 10)).isDisplayed());

        boardPage.archiveAllLists();
        assertEquals(0, boardPage.getLists().size());
    }

    @Test
    @Ignore
    public void testWhenOneListWithOneCardWithMaxLengthTextCreatedAndArchived() throws IOException {
        String cardText = Resources.toString(Resources.getResource("txt\\lorem16424.txt"), StandardCharsets.UTF_8);

        boardPage.addList("list");
        assertTrue(boardPage.getListByTitle("list").isDisplayed());

        boardPage.addCard("card");
        assertTrue(boardPage.getCardByTitle("card").isDisplayed());

        boardPage.editCard("card", cardText);

        boardPage.archiveAllLists();
        assertEquals(0, boardPage.getLists().size());
    }

}
