package ru.samara.tests;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.*;
import ru.samara.pages.BoardPage;
import ru.samara.pages.BoardsListPage;
import ru.samara.pages.LoginPage;
import ru.samara.pages.MainPage;

import static java.util.concurrent.TimeUnit.SECONDS;

public class BaseTest {

    public WebDriver driver;

    protected MainPage mainPage;
    protected LoginPage loginPage;
    protected BoardsListPage boardsListPage;
    protected BoardPage boardPage;

    @BeforeMethod
    public void setUp() {
        driver = new ChromeDriver(new ChromeOptions());
        driver.manage().timeouts().implicitlyWait(2, SECONDS);
        driver.manage().timeouts().pageLoadTimeout(5, SECONDS);
        driver.manage().window().setSize(new Dimension(1920, 1080));

        mainPage = new MainPage(driver);
        loginPage = mainPage.open().openLoginPage();
        boardsListPage = loginPage.loginAs("xowab20406@4tmail.com", "xowab20406");
        boardPage = boardsListPage.openBoard("Добро пожаловать в Trello!");
    }

    @AfterMethod
    public void tearDown() {
        driver.quit();
    }

}
